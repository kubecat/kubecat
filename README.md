# KubeCat



## Leírás

Játssz a barátaiddal egy közös célért. Minél magasabb szintű pályákon vagy, egyre több akadállyal találkozol. Próbáljátok meg cicáitokkal végigjátszani az összes szintet.
Egy többszemélyes platformer, logikai játékot szeretnénk elkészíteni Pythonban (Pygame használatával), amelyben változatos pályákat kell megoldani. A pályák egyre nehezebbé válnak és új pályaelemek kerülnek be a játékba (pl.: mozgó egységek, nagyobb ugrás, stb). A játékot modulárisan, azaz könnyen továbbfejleszthetően szeretnénk elkészíteni, hogy később legyen lehetőség azt bővíteni. A verziókövetéshez GitLabot tervezünk használni, illetve scrumhoz és agilis fejlesztéshez Jira-t.

## Telepítés

***FIGYELEM:** a következő parancsok Windows operációs rendszeren lettek tesztelve és használva.*

A fejlesztést bármilyen fejlesztői környezetben lehet folytatni.
A fejlesztéshez python 3.9.7-et használunk, így ajánlott annak feltelepítése.

A fejlesztéshez python virtuális környezetet használunk, hogy a fejlesztő egyénileg telepített könyvtárai ne befolyásolják a fejlesztést.

A repo gyökérkönyvtárában indítsunk egy parancssort. Majd kövessük az utasítások a fejlesztői környezet sikeres feltelepítéséhez.

Először létre kell hoznunk a virtuális környezetet.
```
python -m venv venv
```

Ezt követően aktiválnunk kell azt.

```
venv\Scripts\activate
```

Majd telepítsük a szükséges könyvtárakat.
```
pip install -r requirements.txt
```

## Futtatás

***FIGYELEM:** a következő parancsok Windows operációs rendszeren lettek tesztelve és használva.*

Ha a telepítés megtörtént, akkor a következőképpen futtathatjuk a játékot.

Fontos, hogy mielőtt elindíthatnánk a játékot először aktiválnunk kell a virtuális környezetet.

```
venv\Scripts\activate
```

Ha utóbbi parancs sikeresen lefutott akkor a paranccsorban vizuálisan látszani fog, hogy a megadott "env" nevű virtuális környezetben vagyunk.

A játék futtatásához, használjuk a következő parancsot.

```
python main.py
```

Amennyiben befejeztük a tesztelést deaktiváljuk a virtuális környezetet a következő parancsal.

```
deactivate
```