from src import spritesheet
import pygame
from src.options import *
from src.physics import *


class Player:
    def __init__(self, x, y):
        self.currentAnimation = "idle"
        self.animations = {"idle": spritesheet.Animation('assets/cat1_idle.png', 100, SCALE),
                           "running": spritesheet.Animation('assets/cat1_running.png', 100, SCALE)}
        self.width = self.animations[self.currentAnimation].spriteSize * SCALE
        self.height = self.animations[self.currentAnimation].spriteSize * SCALE
        self.rect = pygame.Rect(0, 0, self.width, self.height - (2 * SCALE))
        self.rect.center = (x, y)
        self.flipX = False
        self.flipY = False
        self.velocity_x = 1  # 1
        self.velocity_y = 1
        self.jump = False

    def move(self, deltatime, game_map, camera_pos, objects):
        pos_x, pos_y = camera_pos
        speed_x = self.velocity_x * deltatime

        key = pygame.key.get_pressed()
        if self.jump:
            speed_y = self.velocity_y * 2 * deltatime
            self.actual_movement([0, speed_y], objects)
            self.velocity_y -= 0.03
            if self.velocity_y < -1:
                self.jump = False
                self.velocity_y = 1
        else:
            speed_y = self.velocity_y * deltatime
            self.actual_movement([0, speed_y], objects)
        if key[pygame.K_a]:
            self.currentAnimation = "running"

            # self.rect.x -= speed
            self.actual_movement([-speed_x, 0], objects)
            pos_x += speed_x
            self.flipX = True
        else:
            self.currentAnimation = "idle"

        if key[pygame.K_d]:
            self.currentAnimation = "running"
            # self.rect.x += speed
            self.actual_movement([speed_x, 0], objects)
            pos_x -= speed_x
            self.flipX = False

        if key[pygame.K_SPACE]:
            self.jump = True

        if key[pygame.K_a] and key[pygame.K_d]:
            self.currentAnimation = "idle"
        #
        if self.rect.x < 0:  # Simple Sides Collision
            self.rect.x = 0  # Reset Player Rect Coord
            pos_x = 512  # Reset Camera Pos Coord
        elif self.rect.x > game_map.map_size[0] - self.width:
            self.rect.x = game_map.map_size[0] - self.width
            pos_x = camera_pos[0]
        #
        return pos_x, pos_y

    def actual_movement(self, movement, objects):
        self.rect.x += movement[0]
        collisions = collision_test(self, objects)
        for obj in collisions:
            if movement[0] > 0:
                self.rect.right = obj.rect.left
            if movement[0] < 0:
                self.rect.left = obj.rect.right

        self.rect.y += movement[1]
        collisions = collision_test(self, objects)
        for obj in collisions:
            if movement[1] > 0:
                self.rect.bottom = obj.rect.top
            if movement[1] < 0:
                self.rect.top = obj.rect.bottom

    def draw(self, game_map):
        self.animations[self.currentAnimation].updateFrame()
        game_map.world.blit(self.animations[self.currentAnimation].getCurrentFrame(self.flipX, self.flipY), self.rect)
        # pygame.draw.rect(screen, (255, 255, 0), self.rect, 2)
