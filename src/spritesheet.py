import pygame

class SpriteSheet():
	def __init__(self, image):
		self.sheet = image

	def get_image(self, frame, width, height, scale):
		image = pygame.Surface((width, height), pygame.SRCALPHA, 32).convert_alpha()
		image.blit(self.sheet, (0, 0), ((frame * width), 0, width, height))
		image = pygame.transform.scale(image, (width * scale, height * scale))

		return image


class Animation():
	def __init__(self, image, speed, scale):
		self.image = pygame.image.load(image).convert_alpha()
		self.sheet = SpriteSheet(self.image)
		self.numFrames = int(self.image.get_width() / self.image.get_height())
		self.animationSpeed = speed
		self.animationList = []
		self.currentFrame = 0
		self.lastUpdate = pygame.time.get_ticks()
		self.spriteSize = self.image.get_height()
		self.scale = scale

		for i in range(0, self.numFrames):
			self.animationList.append(self.sheet.get_image(i, self.spriteSize, self.spriteSize, self.scale))

	def getCurrentFrame(self, flipX = False, flipY = False):
		return pygame.transform.flip(self.animationList[self.currentFrame], flipX, flipY)

	def updateFrame(self):
		currentTime = pygame.time.get_ticks()
		if currentTime - self.lastUpdate >= self.animationSpeed:
			self.currentFrame += 1
			if self.currentFrame >= len(self.animationList):
				self.currentFrame = 0
			self.lastUpdate = currentTime