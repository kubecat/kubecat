import pygame


def collision_test(player, objects):
    collisions = []
    for obj in objects:
        if pygame.Rect.colliderect(player.rect, obj.rect):
            collisions.append(obj)
    return collisions
