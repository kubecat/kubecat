import sys

import pygame


class MenuScreen:
    def __init__(self, width, height, clock):
        self.width = width
        self.height = height
        self.clock = clock
        self.font = pygame.font.Font('assets/fonts/ARCADECLASSIC.TTF', 60)
        self.background_image = pygame.image.load('assets/background.png', )
        self.menu_index = 1
        self.previous_time_menu = 0
        self.option_texts = ["start game", "settings", "exit"]
        self.menu_options = list(zip(self.option_texts, range(len(self.option_texts))))

    def __draw_text(self, text, font, color, surface, x, y):
        text_obj = font.render(text, 1, color)
        text_rect = text_obj.get_rect()
        text_rect.topleft = (x, y)
        surface.blit(text_obj, text_rect)

    def show(self, display):
        intro = True
        print(self.menu_options)
        while intro:
            #print("main menu is running")
            #key = pygame.key.get_pressed()
            key = []
            if self.menu_index < 0:
                self.menu_index = 1
                key = [lambda x: False for x in range(len(pygame.key.get_pressed()))]
            else:
                key = pygame.key.get_pressed()

            display.blit(self.background_image, (0, 0))
            # prints main menu sign
            self.__draw_text('main menu', self.font, (255, 255, 255), display, 60, 60)
            # prints option names
            for i in range(len(self.menu_options)):
                self.__draw_text(self.menu_options[i][0], self.font, (255, 255, 255), display, 100,
                                 60 + ((self.menu_options[i][1] + 1) * 100))
            index_arrow = pygame.Rect(20, 60 + (self.menu_index * 100), 50, 50)
            now_menu = pygame.time.get_ticks()
            if key[pygame.K_s] and (now_menu > self.previous_time_menu + 500):
                if self.menu_index < len(self.menu_options):
                    self.menu_index = self.menu_index + 1
                    self.previous_time_menu = pygame.time.get_ticks()
                elif self.menu_index == len(self.menu_options):
                    self.previous_time_menu = pygame.time.get_ticks()
            elif key[pygame.K_w] and (now_menu > self.previous_time_menu + 100):
                if self.menu_index >= 2:
                    self.menu_index = self.menu_index - 1
                    self.previous_time_menu = pygame.time.get_ticks()
            elif key[pygame.K_RETURN]:
                if self.menu_index == 1:
                    return 1

                elif self.menu_index == 2:
                    print("in menu-settings")
                    self.__options(display)
                    self.menu_index = (-2)


                elif self.menu_index == 3:
                    print("exiting...")
                    pygame.quit()
                    sys.exit()
                ''' elif self.menu_index == (-2):
                    self.menu_index = 1
                    print("ki vagy lépve a optionsből ")'''

            index_arrow = pygame.Rect(20, 60 + (self.menu_index * 100), 50, 50)
            pygame.draw.rect(display, (255, 255, 255), index_arrow)
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()

            key = []
            pygame.display.update()
            self.clock.tick(60)

    def __options(self, display):
        running = True
        menu_index = 1
        previous_time_menu = 0
        on_off = "on"
        back_pressed = False
        while running:
            #print("option is running")
            display.blit(self.background_image, (0, 0))
            self.__draw_text('options', self.font, (255, 255, 255), display, 20, 20)
            menus = [("music", 1), ("back", 2)]
            toggle = None

            for i in range(0, len(menus)):
                self.__draw_text(menus[i][0], self.font, (255, 255, 255), display, 100, 60 + (menus[i][1] * 100))
                if menus[i][0] != "back":
                    toggle_background = pygame.Rect(300, 60 + (menus[i][1] * 100) + 15, 100, 15)
                    on_toggle = pygame.Rect(350, 60 + (menus[i][1] * 100), 50, 45)
                    off_toggle = pygame.Rect(300, 60 + (menus[i][1] * 100), 50, 45)
                    pygame.draw.rect(display, (128, 128, 128), toggle_background)
                    if on_off == "on":
                        toggle = on_toggle
                        pygame.draw.rect(display, (0, 255, 0), on_toggle)
                    else:
                        toggle = off_toggle
                        pygame.draw.rect(display, (255, 0, 0), off_toggle)

            index_arrow = pygame.Rect(20, 60 + (menu_index * 100), 50, 50)
            now_menu = pygame.time.get_ticks()
            key = pygame.key.get_pressed()

            if key[pygame.K_s] and (now_menu > previous_time_menu + 500):
                if menu_index < len(menus):
                    menu_index = menu_index + 1
                    previous_time_menu = pygame.time.get_ticks()
                elif menu_index == len(menus):
                    previous_time_menu = pygame.time.get_ticks()
            elif key[pygame.K_w] and (now_menu > previous_time_menu + 500):
                if menu_index >= 2:
                    menu_index = menu_index - 1
                    previous_time_menu = pygame.time.get_ticks()
            elif key[pygame.K_RETURN] and (now_menu > previous_time_menu + 500):
                if menu_index == 2:
                    print("back pressed")
                    back_pressed = True
                    running = False
                    break
                    # previous_time_menu = pygame.time.get_ticks()
                    print("go back")
                elif menu_index == 1:
                    if on_off == "on":
                        toggle = pygame.Rect(350, 60 + (menus[0][1] * 100), 50, 45)
                        on_off = "off"
                        print("off")
                    elif on_off == "off":
                        on_off = "on"
                        toggle = pygame.Rect(300, 60 + (menus[0][1] * 100), 50, 45)
                        print("on")
                    previous_time_menu = pygame.time.get_ticks()

            pygame.draw.rect(display, (255, 255, 255), index_arrow)
            if on_off == "on":
                pygame.draw.rect(display, (0, 255, 0), toggle)
            else:
                pygame.draw.rect(display, (255, 0, 0), toggle)

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()
                elif event.type == pygame.KEYDOWN:
                    if back_pressed or event.key == pygame.K_ESCAPE:
                        return

            pygame.display.update()
            self.clock.tick(60)
        pygame.display.flip()
        pygame.display.update()
        self.clock.tick(60)
