import pygame
from src.options import *


class Block:
    def __init__(self, x, y):
        self.width = default_width * SCALE
        self.height = default_height * SCALE
        self.rect = pygame.Rect(0, 0, self.width, self.height)
        self.rect.center = (x, y)

    def draw(self, game_map):
        pygame.draw.rect(game_map.world, (255, 255, 0), self.rect)
        # game_map.world.blit(pygame.Surface((16, 16)), self.rect)
