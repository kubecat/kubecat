import pygame
import sys
from src.player import Player
from src.map import Map
from src.menu import MenuScreen
from src.block import Block
from pygame.locals import *

pygame.init()

SCREEN_WIDTH = 1280
SCREEN_HEIGHT = 720
SCALE = 8

screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
pygame.display.set_caption('Spritesheets')

font = pygame.font.Font('assets/fonts/ARCADECLASSIC.TTF', 60)
background_image = pygame.image.load('assets/background.png', )

BG = (200, 200, 200)
clock = pygame.time.Clock()
FPS = 244


if __name__ == "__main__":

    def draw_text(text, font, color, surface, x, y):
        textobj = font.render(text, 1, color)
        textrect = textobj.get_rect()
        textrect.topleft = (x, y)
        surface.blit(textobj, textrect)


    def main_menu():
        menu = MenuScreen(SCREEN_WIDTH, SCREEN_HEIGHT, clock)
        start = menu.show(screen)
        if start == 1:
            game()


    def game():

        player = Player(SCREEN_WIDTH // 2, SCREEN_HEIGHT - 150)
        objects = [Block(SCREEN_WIDTH // 2 - 256, SCREEN_HEIGHT - 150+128), Block(SCREEN_WIDTH // 2 - 128, SCREEN_HEIGHT - 150+128), Block(SCREEN_WIDTH // 2, SCREEN_HEIGHT - 150+128), Block(SCREEN_WIDTH // 2 - 400, SCREEN_HEIGHT - 150)]  # includes walls, blocks and other items

        game_map = Map(1)
        camera_pos = (0, 0)
        prevTime = pygame.time.get_ticks()
        dt = 0
        run = True
        while run:
            # fps cap
            clock.tick(FPS)
            now = pygame.time.get_ticks()
            dt = now - prevTime
            prevTime = now

            # update background
            screen.fill(BG)

            # cat1.updateFrame()
            # screen.blit(cat1.getCurrentFrame(), (0, 0))

            camera_pos = player.move(dt, game_map, camera_pos, objects)
            game_map.draw()
            player.draw(game_map)

            for obj in objects:
                obj.draw(game_map)

            screen.blit(game_map.world, camera_pos)

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    run = False
                    print("want to exit")

            pygame.display.update()

        '''pygame.quit()'''


    main_menu()
